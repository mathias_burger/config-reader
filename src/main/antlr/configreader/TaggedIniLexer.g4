lexer grammar TaggedIniLexer;

NEWLINE : '\n' | '\r\n' | '\r' ;
WS : [ \t] ;
KEY : ~([\n\r\t =.|;] | '[' | ']')+ ;
SEP : WS* [=] WS* -> pushMode(STRING_MODE) ;
SECTION_OPEN : '[' -> pushMode(SECTION_MODE) ;
TAGS_BEGIN : '.' -> pushMode(TAGS_MODE) ;
LINE_COMMENT : ';' ~[\r\n]+ ('\n' | '\r\n' | '\r') ;
ERR : . ; // prevent lexer gaps

mode STRING_MODE ;
VALUE : ~[\n\r]+ ;
END_VALUE : ('\n' | '\r\n' | '\r') -> popMode ;
ERR_STRING_MODE : . ; // prevent lexer gaps

mode SECTION_MODE ;
SECTION_NAME : ~']'+ ;
SECTION_CLOSE : ']' -> popMode ;
ERR_SECTION_MODE : . ; // prevent lexer gaps

mode TAGS_MODE ;
TAG_NAME : ~[\r\n|]+ ;
TAGS_ALTERNATIVE : '|' ;
TAGS_END : ('\n' | '\r\n' | '\r') -> popMode ;
ERR_TAGS_MODE : . ; // prevent lexer gaps