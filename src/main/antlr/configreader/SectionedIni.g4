grammar SectionedIni;
options {
    tokenVocab=SectionedIniLexer;
}

file : row+ NEWLINE* EOF;
row
    : SECTION_OPEN section_name=.*? SECTION_CLOSE  # Section
    | KEY SEP VALUE END_VALUE                      # KeyValue
    | KEY SEP       END_VALUE                      # KeyEmptyValue
    | KEY           NEWLINE                        # KeyOnly
    | NEWLINE                                      # EmptyLine
    ;
