package configreader.streams;

import org.antlr.v4.runtime.IntStream;
import org.antlr.v4.runtime.UnbufferedCharStream;

import java.io.IOException;
import java.io.Reader;

public class UnbufferedCharStreamWithTrailingNewline extends UnbufferedCharStream {
    private boolean atEnd = false;

    public UnbufferedCharStreamWithTrailingNewline(Reader input) {
        super(input);
    }

    @Override
    protected int nextChar() throws IOException {
        if (atEnd)
            return IntStream.EOF;

        int character = super.nextChar();
        if (character == IntStream.EOF) {
            atEnd = true;
            return '\n';
        }

        return character;
    }
}
