# README #

An artifact to satisfy most configuration file writing and reading needs.

### Vision ###

* Provide a polyglot configuration file reader in Java, PHP and other languages
* Support multiple configuration file formats
* Support variables
* Support an extension mechanism
* Provide an IDE plugin to efficiently modify and refactor entries

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Open an issue and provide a pull request resolving the issue
* Writing tests
* Code review
