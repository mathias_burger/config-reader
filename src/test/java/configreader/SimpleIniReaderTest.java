package configreader;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;

import static org.assertj.core.api.Assertions.assertThat;

public class SimpleIniReaderTest {

    private static final String SIMPLE_INI_FILE = "src/test/resources/fixtures/SimpleIni.ini";
    private SimpleIniReader iniReader;

    @Before
    public void setUp() throws FileNotFoundException {
        iniReader = new SimpleIniReader(new FileReader(SIMPLE_INI_FILE));
    }

    @Test
    public void readExistingValues() {
        assertThat(iniReader.getValue("key")).isEqualTo("value");
        assertThat(iniReader.getValue("key2")).isEqualTo("value = blah");
        assertThat(iniReader.getValue("key3")).isEqualTo("value  =  blah");
        assertThat(iniReader.getValue("key4")).isEqualTo("value = blah = blubb");
        assertThat(iniReader.getValue("key5")).isEqualTo("");
        assertThat(iniReader.getValue("key6")).isEqualTo("");
        assertThat(iniReader.getValue("key7")).isEqualTo("my cool text");
        assertThat(iniReader.getValue("key8")).isEqualTo("my super  cool  =   text is  good");
        assertThat(iniReader.getValue("key9")).isEqualTo("strip leading whitespace from key");
    }

    @Test
    public void readNonExistingValue() {
        assertThat(iniReader.getValue("inexisting")).isNull();
    }
}