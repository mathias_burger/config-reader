lexer grammar SectionedIniLexer;

NEWLINE : '\n' | '\r\n' | '\r' ;
WS : [ \t] -> skip ;
KEY : ~([\n\r= \t] | '[' | ']')+ ;
SEP : [=] WS* -> pushMode(STRING_MODE) ;
SECTION_OPEN : '[' ;
SECTION_CLOSE : ']' ;

mode STRING_MODE ;
VALUE : ~[\n\r]+ ;
END_VALUE : [\n\r]+ -> popMode ;