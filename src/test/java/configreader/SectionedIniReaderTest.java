package configreader;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;

import static org.assertj.core.api.Assertions.assertThat;

public class SectionedIniReaderTest {

    private static final String SECTIONED_INI_FILE = "src/test/resources/fixtures/SectionedIni.ini";
    private SectionedIniReader iniReader;

    @Before
    public void setUp() throws FileNotFoundException {
        iniReader = new SectionedIniReader(new FileReader(SECTIONED_INI_FILE));
    }

    @Test
    public void readExistingValuesInSectionedIniFile() {
        assertThat(iniReader.getValueInSection("", "key")).isEqualTo("value");
        assertThat(iniReader.getValueInGlobalSection("key")).isEqualTo("value");
        assertThat(iniReader.getValueInSection("section1", "key2")).isEqualTo("value = blah");
        assertThat(iniReader.getValueInSection("section1", "key3")).isEqualTo("value  =  blah");
        assertThat(iniReader.getValueInSection("section2", "key4")).isEqualTo("value = blah = blubb");
        assertThat(iniReader.getValueInSection("section2", "key5")).isEqualTo("");
        assertThat(iniReader.getValueInSection("section2", "key6")).isEqualTo("");
        assertThat(iniReader.getValueInSection("section2", "key7")).isEqualTo("my cool text");
        assertThat(iniReader.getValueInSection("section2", "key8")).isEqualTo("my super  cool  =   text is  good");
        assertThat(iniReader.getValueInSection("section3", "key9")).isEqualTo("strip leading whitespace from key");
    }

    @Test
    public void readNonExistingValue() {
        assertThat(iniReader.getValueInGlobalSection("inexisting")).isNull();
        assertThat(iniReader.getValueInSection("", "key2")).isNull();
        assertThat(iniReader.getValueInGlobalSection("key2")).isNull();
        assertThat(iniReader.getValueInSection("section1", "key4")).isNull();
    }
}