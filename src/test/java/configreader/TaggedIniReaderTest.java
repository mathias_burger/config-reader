package configreader;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.io.FileReader;

import static org.assertj.core.api.Assertions.assertThat;

public class TaggedIniReaderTest {

    private static final String TAGGED_INI_FILE = "src/test/resources/fixtures/TaggedIni.ini";
    private TaggedIniReader iniReader;

    @Before
    public void setUp() throws FileNotFoundException {
        iniReader = new TaggedIniReader(new FileReader(TAGGED_INI_FILE));
    }

    @Test
    public void readExistingValuesInTaggedIniFile() {
        assertThat(iniReader.getValueInGlobalSection("key")).isEqualTo("value");
        assertThat(iniReader.getValueInSection("", "key")).isEqualTo("value");
        assertThat(iniReader.getTaggedValueInSection("", "", "key")).isEqualTo("value");
        assertThat(iniReader.getTaggedValueInGlobalSection("tag", "key")).isEqualTo("v");
        assertThat(iniReader.getTaggedValueInSection("", "", "key")).isEqualTo("value");
        assertThat(iniReader.getTaggedValueInSection("", "section1", "key2")).isEqualTo("value = blah");
        assertThat(iniReader.getValueInSection("section1", "key2")).isEqualTo("value = blah");
        assertThat(iniReader.getTaggedValueInSection("abc", "section1", "key3")).isEqualTo("value  =  blah");
        assertThat(iniReader.getTaggedValueInSection("tbd", "section2", "key4")).isEqualTo("value = blah = blubb");
        assertThat(iniReader.getTaggedValueInSection("tbd", "section2", "key5")).isEqualTo("");
        assertThat(iniReader.getTaggedValueInSection("tbd", "section2", "key6")).isEqualTo("");
        assertThat(iniReader.getTaggedValueInSection("tdd", "section2", "key6")).isEqualTo("");
        assertThat(iniReader.getTaggedValueInSection("tbd", "section2", "key7")).isEqualTo("my cool text");
        assertThat(iniReader.getTaggedValueInSection("tdd", "section2", "key7")).isEqualTo("my cool text");
        assertThat(iniReader.getTaggedValueInSection("tldr", "section2", "key8")).isEqualTo("my super  cool  =   text is  good");
        assertThat(iniReader.getTaggedValueInSection("", "section3", "key9")).isEqualTo("strip leading whitespace from key");
        assertThat(iniReader.getTaggedValueInSection("", "section4", "key10")).isEqualTo("untagged");
        assertThat(iniReader.getTaggedValueInSection("tbd", "section4", "key10")).isEqualTo("tagged");
        assertThat(iniReader.getValueInSection("section4", "key10")).isEqualTo("untagged");
    }

    @Test
    public void readNonExistingValue() {
        assertThat(iniReader.getValueInGlobalSection("inexisting")).isNull();
        assertThat(iniReader.getValueInGlobalSection("key2")).isNull();
        assertThat(iniReader.getValueInSection("", "key2")).isNull();
        assertThat(iniReader.getValueInSection("section1", "key4")).isNull();
        assertThat(iniReader.getTaggedValueInSection("","", "key2")).isNull();
        assertThat(iniReader.getTaggedValueInSection("","section1", "key3")).isNull();
        assertThat(iniReader.getTaggedValueInSection("inexisting","section1", "key2")).isNull();
    }

    @Test
    public void commentedValueIsUnavailable() {
        assertThat(iniReader.getValueInGlobalSection(";kex")).isNull();
    }
}