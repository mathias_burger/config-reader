package configreader;

import configreader.streams.UnbufferedCharStreamWithTrailingNewline;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.Reader;
import java.util.LinkedHashMap;
import java.util.Map;

public class SectionedIniReader {
    private static final String GLOBAL_SECTION = "";

    private boolean initialized = false;
    private Reader reader;
    private Map<String, Map<String, String>> config;

    public SectionedIniReader(Reader reader) {
        this.reader = reader;
    }

    public String getValueInGlobalSection(String key) {
        init();
        return config.get(GLOBAL_SECTION).get(key);
    }

    public String getValueInSection(String section, String key) {
        init();
        return config.get(section).get(key);
    }

    private void init() {
        if (!initialized) {
            SectionedIniLexer lexer = new SectionedIniLexer(new UnbufferedCharStreamWithTrailingNewline(reader));
            lexer.setTokenFactory(new CommonTokenFactory(true));

            SectionedIniParser parser = new SectionedIniParser(new CommonTokenStream(lexer));
            SectionedIniVisitor visitor = new SectionedIniVisitor();

            visitor.visit(parser.file());
            config = visitor.getConfig();

            initialized = true;
        }
    }

    private static class SectionedIniVisitor extends SectionedIniBaseVisitor<Void> {
        private static final String EMPTY_VALUE = "";
        private Map<String, Map<String, String>> config;
        private String currentSection = GLOBAL_SECTION;

        public SectionedIniVisitor() {
            config = new LinkedHashMap<>();
            config.put(GLOBAL_SECTION, new LinkedHashMap<>());
        }

        @Override
        public Void visitSection(SectionedIniParser.SectionContext ctx) {
            currentSection = ctx.section_name.getText();
            config.put(currentSection, new LinkedHashMap<>());
            return null;
        }

        @Override
        public Void visitKeyOnly(SectionedIniParser.KeyOnlyContext ctx) {
            config.get(currentSection).put(ctx.KEY().getText(), EMPTY_VALUE);
            return null;
        }

        @Override
        public Void visitKeyEmptyValue(SectionedIniParser.KeyEmptyValueContext ctx) {
            config.get(currentSection).put(ctx.KEY().getText(), EMPTY_VALUE);
            return null;
        }

        @Override
        public Void visitKeyValue(SectionedIniParser.KeyValueContext ctx) {
            config.get(currentSection).put(ctx.KEY().getText(), ctx.VALUE().getText());
            return null;
        }

        public Map<String, Map<String, String>> getConfig() {
            return config;
        }
    }
}
