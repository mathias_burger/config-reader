package configreader;

import configreader.streams.UnbufferedCharStreamWithTrailingNewline;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;

import java.io.Reader;
import java.util.LinkedHashMap;
import java.util.Map;

public class SimpleIniReader {

    private boolean initialized = false;
    private Reader reader;
    private Map<String, String> config = new LinkedHashMap<>();

    public SimpleIniReader(Reader reader) {
        this.reader = reader;
    }

    public String getValue(String key) {
        init();
        return config.get(key);
    }

    private void init() {
        if (!initialized) {
            SimpleIniLexer lexer = new SimpleIniLexer(new UnbufferedCharStreamWithTrailingNewline(reader));
            lexer.setTokenFactory(new CommonTokenFactory(true));

            SimpleIniParser parser = new SimpleIniParser(new CommonTokenStream(lexer));
            SimpleIniVisitor visitor = new SimpleIniVisitor();

            visitor.visit(parser.file());
            config = visitor.getConfig();

            initialized = true;
        }
    }

    private static class SimpleIniVisitor extends SimpleIniBaseVisitor<Void> {
        private Map<String, String> config = new LinkedHashMap<>();

        @Override
        public Void visitKeyOnly(SimpleIniParser.KeyOnlyContext ctx) {
            config.put(ctx.key().getText(), "");
            return null;
        }

        @Override
        public Void visitKeyValue(SimpleIniParser.KeyValueContext ctx) {
            config.put(ctx.key().getText(), ctx.value().getText());
            return null;
        }

        public Map<String, String> getConfig() {
            return config;
        }
    }
}
