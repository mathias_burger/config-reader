package configreader;

import configreader.streams.UnbufferedCharStreamWithTrailingNewline;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.io.Reader;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class TaggedIniReader {
    private static final String GLOBAL_SECTION = "";
    private static final String NO_TAG = "";

    private boolean initialized = false;
    private Reader reader;
    private Map<String, Map<String, Map<String, String>>> config;

    public TaggedIniReader(Reader reader) {
        this.reader = reader;
    }

    public String getValueInGlobalSection(String key) {
        init();
        return getTaggedValueInSection(NO_TAG, GLOBAL_SECTION, key);
    }

    public String getValueInSection(String section, String key) {
        init();
        return getTaggedValueInSection(NO_TAG, section, key);
    }

    public String getTaggedValueInGlobalSection(String tag, String key) {
        init();
        return getTaggedValueInSection(tag, GLOBAL_SECTION, key);
    }

    public String getTaggedValueInSection(String tag, String section, String key) {
        init();
        if (!config.containsKey(section) || !config.get(section).containsKey(tag)) return null;
        return config.get(section).get(tag).get(key);
    }

    private void init() {
        if (!initialized) {
            TaggedIniLexer lexer = new TaggedIniLexer(new UnbufferedCharStreamWithTrailingNewline(reader));
            lexer.setTokenFactory(new CommonTokenFactory(true));

            TaggedIniParser parser = new TaggedIniParser(new CommonTokenStream(lexer));
            TaggedIniVisitor visitor = new TaggedIniVisitor();

            visitor.visit(parser.file());
            config = visitor.getConfig();

            initialized = true;
        }
    }

    private static class TaggedIniVisitor extends TaggedIniBaseVisitor<Void> {
        private static final String EMPTY_VALUE = "";
        private Map<String, Map<String, Map<String, String>>> config; // section, tag, key
        private String currentSection = GLOBAL_SECTION;
        private Set<String> currentTags = new LinkedHashSet<>();

        public TaggedIniVisitor() {
            config = new LinkedHashMap<>();
            currentTags.add(NO_TAG);
            createConfigSection();
        }

        @Override
        public Void visitSection(TaggedIniParser.SectionContext ctx) {
            currentSection = ctx.SECTION_NAME().getText();
            currentTags = new LinkedHashSet<>();
            currentTags.add(NO_TAG);
            createConfigSection();
            return super.visitSection(ctx);
        }

        private void createConfigTag(String tag) {
            if (!config.get(currentSection).containsKey(tag)) {
                config.get(currentSection).put(tag, new LinkedHashMap<>());
            }
        }

        private void createConfigSection() {
            config.put(currentSection, new LinkedHashMap<>());
            config.get(currentSection).put(NO_TAG, new LinkedHashMap<>());
        }

        @Override
        public Void visitKeyOnly(TaggedIniParser.KeyOnlyContext ctx) {
            for (String tag : currentTags) {
                config.get(currentSection).get(tag).put(ctx.key().getText(), EMPTY_VALUE);
            }
            return super.visitKeyOnly(ctx);
        }

        @Override
        public Void visitKeyEmptyValue(TaggedIniParser.KeyEmptyValueContext ctx) {
            for (String tag : currentTags) {
                config.get(currentSection).get(tag).put(ctx.key().getText(), EMPTY_VALUE);
            }
            return super.visitKeyEmptyValue(ctx);
        }

        @Override
        public Void visitKeyValue(TaggedIniParser.KeyValueContext ctx) {
            for (String tag : currentTags) {
                config.get(currentSection).get(tag).put(ctx.key().getText(), ctx.VALUE().getText());
            }
            return super.visitKeyValue(ctx);
        }

        @Override
        public Void visitTags(TaggedIniParser.TagsContext ctx) {
            currentTags = new LinkedHashSet<>();
            for (TerminalNode node : ctx.getTokens(TaggedIniLexer.TAG_NAME)) {
                String tag = node.getText();
                createConfigTag(tag);
                currentTags.add(tag);
            }
            return super.visitTags(ctx);
        }

        public Map<String, Map<String, Map<String, String>>> getConfig() {
            return config;
        }
    }
}
