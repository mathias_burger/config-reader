grammar TaggedIni;
options {
    tokenVocab=TaggedIniLexer;
}

file : row+ EOF;
row
    : section
    | keyValue
    | keyEmptyValue
    | tags
    | keyOnly
    | emptyLine
    | lineComment
    ;

section : SECTION_OPEN SECTION_NAME SECTION_CLOSE NEWLINE ;
keyValue : key SEP VALUE END_VALUE ;
keyEmptyValue : key SEP END_VALUE ;
tags : TAGS_BEGIN TAG_NAME (TAGS_ALTERNATIVE TAG_NAME)* TAGS_END ;
keyOnly : key NEWLINE ;
emptyLine : NEWLINE ;
lineComment : LINE_COMMENT ;
key : KEY ;