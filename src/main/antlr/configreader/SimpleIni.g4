grammar SimpleIni;

file
    : NEWLINE* (row NEWLINE+)* row NEWLINE?
    |
    ;

row : WS? key WS? '=' WS? value  # KeyValue
    | WS? key                    # KeyOnly
    ;

value
    : TEXT (WS? '='? WS? TEXT)*
    |
    ;
key : TEXT ;

NEWLINE : '\n' | '\r\n' | '\r' ;
WS : [ \t]+ ;
TEXT : ~[\n\r= ]+ ;
